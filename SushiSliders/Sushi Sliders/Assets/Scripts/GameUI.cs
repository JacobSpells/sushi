using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    public void NextLevel()
    {
        TileGenerator.instance.NextLevel();
    }

    public void RestartLevel()
    {
        GameManager.instance.RestartLevel();
    }
}
