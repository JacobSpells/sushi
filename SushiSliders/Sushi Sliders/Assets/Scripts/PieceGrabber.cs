using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceGrabber : MonoBehaviour
{
    public GameObject curpiece;
    private void Update()
    {
        gameObject.transform.position = new Vector2(GameObject.FindGameObjectWithTag("MainCamera").transform.position.x + 1.53f, 
            GameObject.FindGameObjectWithTag("MainCamera").transform.position.y - 3.69f);

        if (Input.GetMouseButtonDown(0))
        {
            if(curpiece != null && !curpiece.GetComponent<MovePiece>().picked)
            {
                Debug.Log(curpiece.GetComponent<MovePiece>().GetType());
                TileGenerator.instance.PiecePicked(curpiece.GetComponent<MovePiece>().GetType());
                curpiece.GetComponent<MovePiece>().picked = true;
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("piece enter");
        curpiece = collision.gameObject;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("piece exit");
        curpiece = null;
    }
}
