using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public GameObject camera;
    [SerializeField] private GameObject Donezo;
    [SerializeField] private GameObject Winner;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        
    }

    public void IsDone()
    {
        Winner.gameObject.SetActive(true);
        Time.timeScale = 0f;
    }
    public void IsDead()
    {
        Player.instance.spriteRenderer.sprite = Resources.Load<Sprite>("Sprites/Player_up_gameover");
        Donezo.gameObject.SetActive(true);
        Time.timeScale = 0f;
    }

    void Update()
    {
        if (camera == null)
            camera = GameObject.FindGameObjectWithTag("MainCamera");
        if(Donezo == null)
            Donezo = GameObject.FindGameObjectWithTag("Donezo");
        if (Winner == null)
            Donezo = GameObject.FindGameObjectWithTag("Winner");

    }

    public void RestartLevel()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ChangeScene(int scenenum)
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(scenenum);
    }
}
