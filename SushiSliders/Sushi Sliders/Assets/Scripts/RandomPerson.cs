using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPerson : MonoBehaviour
{
    public Sprite[] sprites;
    [SerializeField] SpriteRenderer spriteRenderer;
    void Start()
    {
        int s = Random.Range(0,sprites.Length);
        spriteRenderer.sprite = sprites[s];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
