using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePiece : MonoBehaviour
{
    //public float pos1, pos2, posy;
    public Vector2 vect1, vect2;
    private float t = 0.0f;
    public float timePeriod = 2.0f;
    [SerializeField] SpriteRenderer spriteRenderer;
    string movetype = "";
    public string[] moveTypes;
    public bool picked = false;

    private void Start()
    {

        //GameObject cam = GameObject.FindGameObjectWithTag("MainCamera");

        //vect1 = new Vector2(cam.transform.position.x + pos1, cam.transform.position.y + posy);
        //vect2 = new Vector2(cam.transform.position.x + pos2, cam.transform.position.y + posy);
        int plumpy = Random.Range(0, moveTypes.Length);
        //SetPiece(moveTypes[plumpy]);
    }

    void Update()
    {
        transform.position = Vector2.Lerp(vect1,vect2,t);

        t += Time.deltaTime / timePeriod;

        if(t > timePeriod)
        {
            Destroy(gameObject);
        }

        if (picked)
            spriteRenderer.color = new Color(1, 1, 1, .5f);
    }

    public void SetPiece(string s)
    {
        movetype = s;
        spriteRenderer.sprite = Resources.Load<Sprite>("Sprites/MovePieces/" + s);
    }

    public string GetType()
    {
        return movetype;
    }
}
