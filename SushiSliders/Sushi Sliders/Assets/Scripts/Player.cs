using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Sprite[] sprites;
    public int turn = 0; // 0 = up, 1 = right,  2 = down, 3 = left
    public static Player instance;
    public int playx, playy;
    Vector3 targetvect;
    List<Vector3> posqueue = new List<Vector3>();
    bool atpos = true;
    public SpriteRenderer spriteRenderer;

    private void Start()
    {
        instance = this;
        targetvect = gameObject.transform.position;
    }

    private void Update()
    {
        if(gameObject.transform.position != targetvect)
        {
            transform.position = Vector3.MoveTowards(transform.position,targetvect,3*Time.deltaTime);

        }
        else
        {
            /*if(posqueue[0] != null)
            {
                targetvect = posqueue[0];
                posqueue.RemoveAt(0);
            }*/
            if (!atpos)
            {
                atpos = true;
                TileGenerator.instance.ActionDone();
            }
        }
    }

    public void Turn(string dir)
    {
        StartCoroutine(turnroutine(dir));
    }

    private void TurnPlayer(string dir)
    {
        if (dir == "Clockwise")
        {
            TurnClockwise();
        }
        else if (dir == "Counter")
        {

            TurnCounterClockwise();
        }
        else if (dir == "TurnAround")
        {
            TurnClockwise();
            TurnClockwise();
        }
    }

    public void TurnClockwise()
    {
        turn++;
        if (turn == 4)
        {
            turn = 0;
        }
        GetComponent<SpriteRenderer>().sprite = sprites[turn];
    }

    public void TurnCounterClockwise()
    {
        turn--;
        if (turn == -1)
        {
            turn = 3;
        }
        GetComponent<SpriteRenderer>().sprite = sprites[turn];
    }

    public void MoveToPos(Vector3 target)
    {
        if (targetvect != gameObject.transform.position)
        {
            posqueue.Add(target);
        }
        else
        {
            targetvect = target;
            atpos = false;
        }
    }

    private void Move()
    {

    }

    IEnumerator turnroutine(string swag)
    {
        TurnPlayer(swag);
        yield return new WaitForSeconds(.25f);
        TileGenerator.instance.ActionDone();
        yield return null;
    }
}
