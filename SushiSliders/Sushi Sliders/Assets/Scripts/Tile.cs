using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public Sprite[] sprites; // 0 ground, 1 wall, 2 hole, 3 Sushi, 4 Customer
    [SerializeField] private SpriteRenderer spriteRenderer;
    public int type,tilex,tiley;
    public GameObject sushi, person;
    public GameObject mySushi, myPerson;
    public bool isSushi = false;
    public bool isCust = false;
    void Start()
    {
        if(sprites == null)
            spriteRenderer = GetComponent<SpriteRenderer>();
    }


    void Update()
    {
        
    }

    public void SetTile(int x, int y)
    {
        tilex = x;
        tiley = y;
    }

    public void SetType(int atype)
    {
        type = atype;
        if(atype < 3)
            spriteRenderer.sprite = sprites[atype];

        if(atype == 3)
        {
            IsSushi();
        }
        if(atype == 4)
        {
            IsCustomer();
        }
    }

    public void IsSushi()
    {
        isSushi = true;
        mySushi = Instantiate(sushi,gameObject.transform);
    }

    public void IsCustomer()
    {
        isCust = true;
        myPerson = Instantiate(person,gameObject.transform);
    }
}
