using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TileGenerator : MonoBehaviour
{
    public int level = 0;
    public GameObject[,] tiles;
    public int boardx = 0;
    public int boardy = 0;
    [SerializeField] GameObject tilePrefab;
    //public Vector2 goal;
    [SerializeField] Vector2[] walls;
    [SerializeField] Vector2[] holes;
    [SerializeField] Vector2[] sushi;
    [SerializeField] Vector2[] customers;
    [SerializeField] Vector2 playerstart;
    [SerializeField] GameObject playerpref;
    public GameObject playerobj;
    Player player;
    bool cammoved = false;
    public static TileGenerator instance;
    public int sushis = 0;
    public int people = 0;
    List<string> actqueue = new List<string>();
    public Image[] images;
    public int platenum = 0;
    public int sushicount;
    public int peoplecount;
    public int method;
    bool moving = false;
    [SerializeField] TMP_Text sush;
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        tiles = new GameObject[boardx, boardy];
        GenerateBoard();
        playerobj = Instantiate(playerpref, tiles[(int)playerstart.x, (int)playerstart.y].transform.position, Quaternion.identity);
        player = playerobj.GetComponent<Player>();
        /*if (method == 0)
            sush.text = "";
        else
            sush.text = "Sushi: 0";*/
    }


    void Update()
    {
        if (!cammoved)
        {
            if (GameManager.instance.camera != null)
            {
                GameManager.instance.camera.transform.position = new Vector3(((float)boardx / 2 - 1) * gameObject.transform.localScale.x, ((float)boardy / 2 - 1) * gameObject.transform.localScale.x, -10);
                cammoved = true;
            }
        }
        if (sush == null)
        {
            sush = GameObject.FindGameObjectWithTag("SushCtr").GetComponent<TMP_Text>();
        }
    }

    public void NextLevel()
    {
        if (level != 5)
            GameManager.instance.ChangeScene(level + 1);
        else
            GameManager.instance.ChangeScene(0);
    }

    public void GenerateBoard()
    {
        float sc = gameObject.transform.localScale.x;
        for (int i = 0; i < boardx; i++)
        {
            for (int j = 0; j < boardy; j++)
            {
                tiles[i, j] = Instantiate(tilePrefab, gameObject.transform);
                tiles[i, j].transform.position = new Vector3((float)i * 1.28f * sc, (float)j * 1.28f * sc, -.1f);
                tiles[i, j].GetComponent<Tile>().SetTile(i, j);
            }
        }
        if (walls.Length > 0)
        {
            for (int i = 0; i < walls.Length; i++)
                tiles[(int)walls[i].x, (int)walls[i].y].GetComponent<Tile>().SetType(1);
        }
        if (holes.Length > 0)
        {
            for (int i = 0; i < holes.Length; i++)
                tiles[(int)holes[i].x, (int)holes[i].y].GetComponent<Tile>().SetType(2);
        }
        for (int i = 0; i < sushi.Length; i++)
        {
            tiles[(int)sushi[i].x, (int)sushi[i].y].GetComponent<Tile>().SetType(3);
            sushis++;
        }
        for (int i = 0; i < customers.Length; i++)
        {
            tiles[(int)customers[i].x, (int)customers[i].y].GetComponent<Tile>().SetType(4);
            people++;
        }
        //tiles[(int)goal.x, (int)goal.y].GetComponent<Tile>().IsGoal();
    }

    public void PiecePicked(string s)
    {
        if (method == 0)
        {
            if (actqueue.Count > 0)
            {
                actqueue.Add(s);
            }
            else
            {
                actqueue.Add(s);
                Action();
            }
        }
        else
        {
            if (moving)
                return;

            actqueue.Add(s);
            /*images[2].sprite = images[1].sprite;
            images[1].sprite = images[0].sprite;
            images[0].sprite = Resources.Load<Sprite>("Sprites/MovePieces/" + s);*/
            Sprite ns = Resources.Load<Sprite>("Sprites/MovePieces/" + s);
            if (images[0].sprite != null)
            {
                if (images[1].sprite != null)
                    images[2].sprite = ns;
                else
                    images[1].sprite = ns;
            }else
                images[0].sprite = ns;

            if (actqueue.Count == 3)
            {
                moving = true;
                Action();
            }
        }
    }



    public void ActionDone()
    {
        if (actqueue[0] != "Forwardx2")
            actqueue.RemoveAt(0);
        else
            actqueue[0] = "Forward";
        Tile curtile = tiles[player.playx, player.playy].GetComponent<Tile>();
        if (curtile.type == 2)
        {
            GameManager.instance.IsDead();
            return;
        }
        else if (curtile.type == 3)
        {
            sushicount++;
            Destroy(curtile.mySushi);
            curtile.type = 0;
            curtile.isSushi = false;
            if (method == 0)
            {
                images[platenum].sprite = Resources.Load<Sprite>("Sprites/sushistill");
                platenum++;
            }
            else
            {
                sush.text = "Sushi: " + sushicount;
            }
        }
        else if (curtile.type == 4)
        {
            if (sushicount > 0)
            {
                people--;
                Destroy(curtile.myPerson);
                curtile.type = 0;
                curtile.isCust = false;
                sushicount--;
                if (method == 0)
                {
                    platenum--;
                    images[platenum].sprite = null;
                }
                else
                {
                    sush.text = "Sushi: " + sushicount;
                }
                if (people == 0)
                {
                    GameManager.instance.IsDone();
                    return;
                }
            }
        }
        if (actqueue.Count > 0)
        {
            Action();
        }
        else
        {
            if (method == 1)
            {
                images[2].sprite = null;
                images[1].sprite = null;
                images[0].sprite = null;

                moving = false;
            }
        }

    }
    public void Action()
    {
        string s = actqueue[0];
        if (s == "Forward" || s == "Forwardx2")
        {
            if (player.turn == 0)
            {
                if (player.playy + 1 >= boardy || tiles[player.playx, (player.playy + 1)].GetComponent<Tile>().type == 1)
                {
                    //don't
                    ActionDone();
                }
                else
                {
                    player.playy += 1;
                    player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                }
            }
            else if (player.turn == 1)
            {
                if (player.playx + 1 >= boardy || tiles[player.playx + 1, (player.playy)].GetComponent<Tile>().type == 1)
                {
                    //don't
                    ActionDone();
                }
                else
                {
                    player.playx += 1;
                    player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                }
            }
            else if (player.turn == 2)
            {
                if (player.playy - 1 < 0 || tiles[player.playx, (player.playy - 1)].GetComponent<Tile>().type == 1)
                {
                    //don't
                    ActionDone();
                }
                else
                {
                    player.playy -= 1;
                    player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                }
            }
            else if (player.turn == 3)
            {
                if (player.playx - 1 < 0 || tiles[player.playx - 1, (player.playy)].GetComponent<Tile>().type == 1)
                {
                    //don't
                    ActionDone();
                }
                else
                {
                    player.playx -= 1;
                    player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                }
            }
        }
        else if (s == "Clockwise")
        {
            player.Turn("Clockwise");
        }
        else if (s == "Counter")
        {
            player.Turn("Counter");
        }
        /*else if (s == "Forwardx2")
        {
            if (player.turn == 0)
            {
                if (player.playy + 2 >= boardy || tiles[player.playx, (player.playy + 2)].GetComponent<Tile>().type == 1)
                {
                    if (player.playy + 1 >= boardy || tiles[player.playx, (player.playy + 1)].GetComponent<Tile>().type == 1)
                    {
                        //don't
                        ActionDone();
                    }
                    else
                    {
                        player.playy += 1;
                        player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                    }
                }
                else
                {
                    player.playy += 1;
                    player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                    player.playy += 1;
                    player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                }
            }
            else if (player.turn == 1)
            {
                if (player.playx + 2 >= boardy || tiles[player.playx + 2, (player.playy)].GetComponent<Tile>().type == 1)
                {
                    if (player.playx + 1 >= boardy || tiles[player.playx + 1, (player.playy)].GetComponent<Tile>().type == 1)
                    {
                        //don't
                        ActionDone();
                    }
                    else
                    {
                        player.playx += 1;
                        player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                    }
                }
                else
                {
                    player.playx += 2;
                    player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                }
            }
            else if (player.turn == 2)
            {
                if (player.playy - 2 < 0 || tiles[player.playx, (player.playy - 2)].GetComponent<Tile>().type == 1)
                {
                    if (player.playy - 1 < 0 || tiles[player.playx, (player.playy - 1)].GetComponent<Tile>().type == 1)
                    {
                        //don't
                        ActionDone();
                    }
                    else
                    {
                        player.playy -= 1;
                        player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                    }
                }
                else
                {
                    player.playy -= 2;
                    player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                }
            }
            else if (player.turn == 3)
            {
                if (player.playx - 2 < 0 || tiles[player.playx - 2, (player.playy)].GetComponent<Tile>().type == 1)
                {
                    //don't
                    if (player.playx - 1 < 0 || tiles[player.playx - 1, (player.playy)].GetComponent<Tile>().type == 1)
                    {
                        //don't
                        ActionDone();
                    }
                    else
                    {
                        player.playx -= 1;
                        player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                    }
                }
                else
                {
                    player.playx -= 2;
                    player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                }
            }
        }*/
        else if (s == "Backward")
        {

            if (player.turn == 0)
            {

                if (player.playy - 1 < 0 || tiles[player.playx, (player.playy - 1)].GetComponent<Tile>().type == 1)
                {
                    //don't
                    ActionDone();
                }
                else
                {
                    player.playy -= 1;
                    player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                }
            }
            else if (player.turn == 1)
            {

                if (player.playx - 1 < 0 || tiles[player.playx - 1, (player.playy)].GetComponent<Tile>().type == 1)
                {
                    //don't
                    ActionDone();
                }
                else
                {
                    player.playx -= 1;
                    player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                }
            }
            else if (player.turn == 2)
            {

                if (player.playy + 1 >= boardy || tiles[player.playx, (player.playy + 1)].GetComponent<Tile>().type == 1)
                {
                    //don't
                    ActionDone();
                }
                else
                {
                    player.playy += 1;
                    player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                }
            }
            else if (player.turn == 3)
            {

                if (player.playx + 1 >= boardy || tiles[player.playx + 1, (player.playy)].GetComponent<Tile>().type == 1)
                {
                    //don't
                    ActionDone();
                }
                else
                {
                    player.playx += 1;
                    player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                }
            }
        }
        else if (s == "Backwardx2")
        {

            if (player.turn == 0)
            {

                if (player.playy - 2 < 0 || tiles[player.playx, (player.playy - 2)].GetComponent<Tile>().type == 1)
                {
                    //don't
                    ActionDone();
                }
                else
                {
                    player.playy -= 2;
                    player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                }
            }
            else if (player.turn == 1)
            {

                if (player.playx - 2 < 0 || tiles[player.playx - 2, (player.playy)].GetComponent<Tile>().type == 1)
                {
                    //don't
                    ActionDone();
                }
                else
                {
                    player.playx -= 2;
                    player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                }
            }
            else if (player.turn == 2)
            {

                if (player.playy - 2 < 0 || tiles[player.playx, (player.playy - 2)].GetComponent<Tile>().type == 1)
                {
                    //don't
                    ActionDone();
                }
                else
                {
                    player.playy -= 2;
                    player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                }
            }
            else if (player.turn == 3)
            {

                if (player.playx + 2 >= boardy || tiles[player.playx + 2, (player.playy)].GetComponent<Tile>().type == 1)
                {
                    //don't
                    ActionDone();
                }
                else
                {
                    player.playx += 2;
                    player.MoveToPos(tiles[player.playx, player.playy].transform.position);
                }
            }
        }
        else if (s == "TurnAround")
        {
            player.Turn("TurnAround");
        }
    }

    public void Restart()
    {
        GameManager.instance.RestartLevel();
    }
}
