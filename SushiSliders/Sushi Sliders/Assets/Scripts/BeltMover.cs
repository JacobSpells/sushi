using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeltMover : MonoBehaviour
{
    [SerializeField] GameObject piece;
    float timer = 0f;
    public string[] piecetypes;
    public float spawnrate, piecespeed;

    private void Update()
    {
        if(timer < 0f)
        {
            string ns = piecetypes[Random.Range(0, piecetypes.Length)];
            GameObject go = Instantiate(piece);
            go.GetComponent<MovePiece>().SetPiece(ns);
            go.GetComponent<MovePiece>().vect1 = gameObject.transform.position;
            go.GetComponent<MovePiece>().vect2 = new Vector2(gameObject.transform.position.x - 14f,gameObject.transform.position.y);
            go.GetComponent<MovePiece>().timePeriod = piecespeed;
            timer = spawnrate;
        }
        timer -= Time.deltaTime;
    }

}
